#### Preparation
make sure you've been installed Node.js on your computer before running this application.

clone this repository

run `npm install` to install all dependecies

run `cp .env.example .env` to copy all environment variable



#### Run The Application
then run `npm start` to run the application. Type `http://localhost:3000/search?search=Batman&page=1` in your favorite browser. The following text after search parameter is movie title.

#### Answer SQL Question
1. Query
   `SELECT p.ID, p.UserName, c.Username as ParentUserName from user p LEFT JOIN user c ON p.Parent = c.ID ORDER BY p.ID`

2. Index berada pada ID
