'use strict'

const express   = require('express')
const bodyParser = require('body-parser')
require('dotenv').config();

const app       = express()

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

const appRoute  = require('./src/Routes/router')(app)

app.listen(process.env.DB_PORT, () => {
    console.log(`Server running on ${process.env.DB_PORT}`)
})