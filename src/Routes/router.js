'use strict'


const omdbController   = require('../Controller/omdbController')

module.exports  =  (app) => {
    app.get('/search', omdbController.search)
}
