'use strict'

const request  = require('request')
require('dotenv').config({ path: '../../.env'})

let controller = {
    
    search : (req, res) => {
        const key   = process.env.OMBD_KEY
        let search  = req.query.search || ''
        let page    = req.query.page || 1
        const url   = process.env.OMBD_URL+`?apikey=${key}&s=${search}&page=${page}`

        request({url}, (error, response, body) => {
            const data  = JSON.parse(body)
            if (error) {
                return res.status(500).send({
                    status: 500,
                    msg: "Internal Server Error"
                })
            } else if (data.Response === 'False') {
                return res.status(404).send({
                    status: 404,
                    msg: data.Error
                })                
            }          
            res.status(response.statusCode).send({
                status: response.statusCode,
                msg: response.statusMessage,
                data: data
            })
        })
        
    }
}

module.exports = controller;